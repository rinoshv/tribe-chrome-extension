window.onload = () => {
    let data = {};
    let commentText = document.querySelector('#comment');
    let source = document.querySelector('#select_quality-text');
    let target = document.querySelector('#select_bad-fit');
    let badFitReason = document.querySelector('#select_bad-fit-text');

    const displayWhenSelected = (source, value, target) => {
        let selectedIndex = source.selectedIndex;
        let isSelected = source[selectedIndex].value === value;

        if (isSelected) {
            target.style.display = 'block';
        } else {
            target.style.display = 'none';
        }
    };

    if (chrome.tabs) {
        chrome.tabs.getSelected(null, (tab) => {
            let tabUrl = tab.url.includes('https://www.linkedin.com/in/');
            if (tabUrl) {
                data.linkedin = [tab.url].map((x) =>x.split('https://www.linkedin.com/in/')[1].split('/')[0])[0];
            } else {
                document.querySelector('#commentform').style.display = 'none';
                document.querySelector('#notLinkedInError').style.display = 'block';
            }
        });
    };

    source.addEventListener('change', () => {
        displayWhenSelected(source, 'Bad fit', target);
        data.quality = source.value;
    });

    commentText.addEventListener('change', () => {
        data.comment = commentText.value;
    });

    badFitReason.addEventListener('change', () => {
        if (source.value === 'Bad fit') {
            data.badFitReason = badFitReason.value;
        }
    });

    document.getElementById('submit').onclick = function fun() {
        if (data.comment && data.quality || data.comment && data.quality && data.badFitReason) {
            document.querySelector('#holder').style.display = 'block';

            get().then(() => {
                document.querySelector('#holder').style.display = 'none';
                clear();
                document.querySelector('#success').style.display = 'block';
                
                setTimeout(() => {
                    document.querySelector('#success').style.display = 'none';
                }, 3000);
            }).catch(() => {
                document.querySelector('#holder').style.display = 'none';
                clear();
            });
            
        }
    }

    function get() { 
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            let Url = 
                `https://tribexyz.bubbleapps.io/api/1.0/wf/linkedin?quality=`+data.quality+
                `&comment=`+data.comment+`&badfitreason=`+data.badFitReason+
                `&linkedin=`+data.linkedin;

            xhr.open('POST', Url, true);
            xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
            xhr.send(JSON.stringify(data));

            xhr.onload = () => {
                if (xhr.status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(Error(xhr.statusText));
                }
            };
        });
    }

    function clear() {
        data = {};
        commentText.value = '';
        source.value = '';
        badFitReason.value = '';
        displayWhenSelected(source, null, target);
    }
}